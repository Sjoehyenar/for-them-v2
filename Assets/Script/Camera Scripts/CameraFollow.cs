﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    private Transform playerTransform;

    public float offset; 

    void Start(){

    // We need to search our tag "Player" to know how he move on the scene (due to playerTransform)
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // called after uptade and fixed uptade 
    void LateUpdate(){

    // Here, it's the "transform" of the camera. We store current camera's position in variable temp - temporary position
        Vector3 temp = transform.position;

    // we set the camera's position x to be equal to the player's position x 
        temp.x = playerTransform.position.x;
        temp.y = playerTransform.position.y;

    // this will add the offset value to the temporary camera x position
        temp.x += offset;
    // this will add the offset value to the temporary camera y position
        temp.y += offset;

    // we set back the camera's temp position to the camera's current position 
        transform.position = temp;
    }
}
